#!/bin/bash
app="registry.gitlab.com/lukasz.bala.tcl/video-review-tool"
docker build -t ${app} .
docker run -d -p 56733:80 --name=${app} -v $PWD:/app ${app}