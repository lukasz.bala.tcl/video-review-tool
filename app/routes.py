import json
import os
import csv
from io import StringIO  # allows you to store response object in memory instead of on disk

from flask import abort, Flask, render_template, request, redirect, url_for, \
    send_from_directory, after_this_request, make_response
from flask_login import login_user, logout_user, current_user, login_required
from app import app
from app.forms import FileForm
from werkzeug import secure_filename
from werkzeug.security import check_password_hash

from app.extensions import db
from app.models import User, Question, Video
from app.utils import load_questions


@app.route('/')
def index():
    form = FileForm()
    files = os.listdir(app.config['UPLOAD_PATH'])
    for name in files:
        if Video.query.filter(Video.name == name).first() is None:
            video = Video(name=name)
            db.session.add(video)
            db.session.commit()
    videos = Video.query.all()
    return render_template('index.html', form=form, current_user=current_user, videos=videos)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        name = request.form['name']
        unhashed_password = request.form['password']

        user = User(
            name=name,
            unhashed_password=unhashed_password,
            admin=False
        )

        db.session.add(user)
        db.session.commit()

        return redirect(url_for('login'))

    return render_template('register.html', current_user=current_user)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        name = request.form['name']
        password = request.form['password']

        user = User.query.filter_by(name=name).first()

        error_message = ''

        if not user or not check_password_hash(user.password, password):
            error_message = 'Could not login. Please check and try again.'

        if not error_message:
            login_user(user)
            return redirect(url_for('index'))
    # Load questions after logging in of user
    return render_template('login.html', current_user=current_user)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/', methods=['POST'])
def upload_file():
    uploaded_file = request.files['file']
    filename = secure_filename(uploaded_file.filename)
    if filename != '':
        file_ext = os.path.splitext(filename)[1]
        if file_ext not in app.config['UPLOAD_EXTENSIONS']:
            abort(400)
        uploaded_file.save(os.path.join(app.config['UPLOAD_PATH'], filename))
    return redirect(url_for('index'))


@app.route('/uploads/<filename>')
def upload(filename):
    return send_from_directory(os.path.abspath(app.config['UPLOAD_PATH']), filename)


@app.route('/ask', methods=['GET', 'POST'])
@login_required
def ask():

    # return redirect(url_for('index'))

    context = {
        'current_user': current_user,
    }

    return render_template('ask.html', **context)


@app.route('/answer/<int:question_id>', methods=['GET', 'POST'])
@login_required
def answer(question_id):
    question = Question.query.get_or_404(question_id)

    if request.method == 'POST':
        question.answer = request.form['answer']
        db.session.commit()

        return redirect(url_for('unanswered'))

    context = {
        'question': question
    }

    return render_template('answer.html', **context)


@app.route('/video/<video_name>', methods=['GET', 'POST'])
@login_required
def video(video_name):
    video = Video.query.filter_by(name=video_name).first()

    if request.method == 'POST':
        pass
        # TODO: fill

    questions = Question.query.all()
    if hasattr(current_user, 'id'):
        load_questions(current_user)

    context = {
        'video': video,
        'current_user': current_user,
        'questions': questions,
    }

    return render_template('video.html', **context)


@app.route('/question/<int:question_id>')
def question(question_id):
    question = Question.query.get_or_404(question_id)

    context = {
        'question': question
    }

    return render_template('question.html', **context)


@app.route('/unanswered')
@login_required
def unanswered():
    unanswered_questions = Question.query \
        .filter(Question.answer == None) \
        .all()

    context = {
        'unanswered_questions': unanswered_questions
    }

    return render_template('unanswered.html', **context)


# export database to csv
# based on https://stackoverflow.com/questions/33766499/flask-button-to-save-table-from-query-as-csv
@app.route('/export/<int:identifier>', methods=['GET'])
def export(load_file_id):
    si = StringIO.StringIO()
    cw = csv.writer(si)
    c = g.db.cursor()
    c.execute('SELECT * FROM TABLE WHERE column_val = :identifier', identifier=identifier)
    rows = c.fetchall()
    cw.writerow([i[0] for i in c.description])
    cw.writerows(rows)
    response = make_response(si.getvalue())
    response.headers['Content-Disposition'] = 'attachment; filename=report.csv'
    response.headers["Content-type"] = "text/csv"
    return response
