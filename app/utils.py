import json

from app.extensions import db
from app.models import User, Question

CLOSED_QUESTIONS_PATH = 'app/questions/questions_closed.json'
OPEN_QUESTIONS_PATH = 'app/questions/questions_open.json'


def load_questions(current_user):
    """
    Loads questions from json file for given logged user.
    :param current_user: current user object taken from flask_login package
    :return:
    """
    with open(CLOSED_QUESTIONS_PATH, encoding='utf-8') as data_file:
        closed = json.loads(data_file.read())

    with open(OPEN_QUESTIONS_PATH, encoding='utf-8') as data_file:
        opened = json.loads(data_file.read())

    for question in closed['questions']:
        question_closed = Question(
            question=question,
            answerer_id=current_user.id
        )
        db.session.add(question_closed)
        db.session.commit()

    for question in opened['questions']:
        question_closed = Question(
            question=question,
            answerer_id=current_user.id
        )
        db.session.add(question_closed)
        db.session.commit()
    return None
