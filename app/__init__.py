import json

from flask import Flask
from config import Config
from flask_migrate import Migrate
from app.commands import create_tables
from app.extensions import db, login_manager
from app.models import User, Question

app = Flask(__name__)
app.config.from_object(Config)
app.cli.add_command(create_tables)
db.init_app(app)

app = Flask(__name__)

app.config.from_object(Config)

db.init_app(app)

login_manager.init_app(app)

login_manager.login_view = 'login'

migrate = Migrate(app, db)

with app.app_context():
    if db.engine.url.drivername == 'sqlite':
        migrate.init_app(app, db, render_as_batch=True)
    else:
        migrate.init_app(app, db)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

from app import routes
