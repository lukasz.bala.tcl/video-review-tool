# Video Review Tool

A tool which allows user to compare two videos (for example with different video bokeh models), add some comments about them and saves final report from the review.

## Instruction

* Environment variables can be set in .flaskenv file

* run "pip install -r requirements.txt"

* One can run flask app using "flask run" command in terminal or use Pycharm and set flask configuration 

## Paths to examples of videos:

/nas/projects/video-bokeh/result-videos-kubernetes/192lippo/192lippo-rgb-box_blur-none-blur_exp-1-radius-15-output_exp-0/

/nas/projects/video-bokeh/result-videos-kubernetes/cocolippo_e192cel2/cocolippo_e192cel2-rgb-box_blur-none-blur_exp-1-radius-15-output_exp-0/

Check videos111, videos132, videos196

ffmpeg command to concatenate videos with a border:

ffmpeg -i input0.mp4 -i input1.mp4 -filter_complex "[0]pad=iw+5:color=black[left];[left][1]hstack=inputs=2" output.mp4