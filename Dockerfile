FROM registry.gitlab.com/tcl-research/internal/general/tcl-base-dev

USER 0:0

# Python
RUN apt-get update -y && \
    apt-get install -y git \
                       software-properties-common \
                       curl \
                       build-essential \
                       libjpeg-dev libpng-dev libtiff-dev \
                       libsm6 libxext6 libxrender-dev \
                       vim \
                       zsh \
                       jq && \
    rm -rf /var/lib/apt/lists/*

RUN git clone --depth 1 https://github.com/junegunn/fzf.git /opt/fzf && \
    /opt/fzf/install --bin && \
    mv /opt/fzf/bin/fzf /usr/bin/ && \
    mv /opt/fzf/shell /usr/share/fzf && \
    rm -rf /opt/fzf

RUN add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get update -y && \
    apt-get remove -y python3-lib2to3 && \
    apt-get install -y python3.8 libpython3.8-dev python3.8-distutils && \
    rm -rf /var/lib/apt/lists/*

RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
    python3.8 get-pip.py && \
    rm get-pip.py

# Make Python 3.8 the default
RUN ln -sf /usr/bin/python3.8 /usr/bin/python3 && \
    ln -sf /usr/bin/python3.8 /usr/bin/python && \
    ln -sf /usr/local/bin/pip3.8 /usr/bin/pip3 && \
    ln -sf /usr/local/bin/pip3.8 /usr/bin/pip

RUN mkdir /app
WORKDIR /app
COPY . /app
RUN pip3 install --upgrade pip && \
    pip3 install --upgrade Cython && \
    pip3 install --upgrade numpy && \
    pip3 install --upgrade pip setuptools wheel && \
    pip3 install -r requirements.txt\
     --no-cache-dir
EXPOSE 5000
CMD ["python", "-m", "flask", "run"]

# This is pathetic
ENV PYTHONIOENCODING=UTF-8
ENV LANG=C.UTF-8
